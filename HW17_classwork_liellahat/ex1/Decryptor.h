#pragma once 

#include "Read_file.h"


class Decryptor
{
public:

	Decryptor(string path);
	void decrypt();
	~Decryptor();

private:

	void increase(int k);
	char getMostUsedChar();
	
	string in;
	string out;
};

