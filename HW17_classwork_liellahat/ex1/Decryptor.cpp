#include "Decryptor.h"


Decryptor::Decryptor(string path)
{
	Read_file r;
	in = r.read("encrypted.txt");
}


void Decryptor::increase(int k)
{
	unsigned int i = 0;

	for (i = 0; i < in.length(); i++)
	{
		if ('a' <= in[i] && in[i] <= 'z')
		{

			out += (in[i] - 'a' + k) % 26 + 'a';
		}
		else
		{
			out += in[i];
		}
	}
}


void Decryptor::decrypt()
{
	int i = 0;
	char mostUsedChar = 0;

	mostUsedChar = getMostUsedChar();
	increase('e' - mostUsedChar);

	cout << out << endl;
}


char Decryptor::getMostUsedChar()
{
	int counters[26] = { 0 };
	unsigned int i = 0;
	int max = 0;

	for (i = 0; i < in.length(); i++)
	{
		if ('a' <= in[i] && in[i] <= 'z')
		{
			counters[in[i] - 'a']++;
		}
	}

	for (i = 0; i < 26; i++)
	{
		if (counters[i] > counters[max])
		{
			max = i;
		}
	}

	return max + 'a';
}


Decryptor::~Decryptor()
{
}