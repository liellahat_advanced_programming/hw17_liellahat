#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];


CryptoDevice::CryptoDevice()
{
	unsigned int i = 0;
	// initializing the key and iv bytes vectors. 
	char keyValue[] = "1234567812345678";
	char ivValue[] = "87654342187654321";

	for (i = 0; i < 16; i++)  // 16 is the default iv and key size.
	{
		key[i] = keyValue[i];
		iv[i] = ivValue[i];
	}
}


CryptoDevice::~CryptoDevice()
{

}


// gets a string and returns the string in AES encryption.
string CryptoDevice::encryptAES(string plainText)
{
	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}


// gets an AES encrypted string and returns the decrypted string.
string CryptoDevice::decryptAES(string cipherText)
{
	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}


// gets a string and returns the string in sha-256 hash encryption. 
string CryptoDevice::sha256(string plainText)
{
	byte* pbData = (byte*)plainText.data();
	unsigned int nDataLen = plainText.size();
	byte abDigest[CryptoPP::SHA256::DIGESTSIZE];
	CryptoPP::HexEncoder encoder;
	string output;

	CryptoPP::SHA256().CalculateDigest(abDigest, pbData, nDataLen);

	encoder.Attach(new CryptoPP::StringSink(output));  // memory will be deleted in the encoder.
	encoder.Put(abDigest, sizeof(abDigest));
	encoder.MessageEnd();

	return output;
}